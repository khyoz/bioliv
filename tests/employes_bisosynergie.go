package main

import (
	// "bioliv/global"
	"bioliv/models"
	"bioliv/modules"
	"encoding/csv"
	"fmt"
	"golang.org/x/text/encoding/charmap"
	"log"
	"os"
	// "strconv"
)

type Xelas struct {
	selas      string
	matricule  string
	lastname   string
	firstname  string
	birthday   string
	sex        string
	embauche   string
	anciennete string
	contract   string
	depart     string
	job        string
}

func main() {
	modules.InitDatabase()
	// records, err := readData("Liste Biosynergie.csv")
	records, err := readData("INSCRITS-BIOGROUP-COMPLET.csv")
	if err != nil {
		log.Fatal(err)
	}
	db := modules.DBConn
	for _, record := range records {
		xselas := Xelas{
			selas:      record[0],
			matricule:  record[1],
			lastname:   record[2],
			firstname:  record[3],
			birthday:   record[4],
			sex:        record[5],
			embauche:   record[6],
			anciennete: record[7],
			contract:   record[8],
			depart:     record[9],
			job:        record[10],
		}

		fmt.Printf("insert %s\n", xselas)
		aselas := models.Employes{Selas: xselas.selas, Matricule: xselas.matricule, Lastname: xselas.lastname, Firstname: xselas.firstname, Birthday: xselas.birthday, Sex: xselas.sex, Embauche: xselas.embauche, Anciennete: xselas.anciennete, Contract: xselas.contract, Depart: xselas.depart, Job: xselas.job}
		result := db.Create(&aselas)
		fmt.Println(result)
	}
}

func readData(fileName string) ([][]string, error) {

	f, err := os.Open(fileName)

	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	// r := csv.NewReader(f)
	r := csv.NewReader(charmap.Windows1252.NewDecoder().Reader(f))

	// skip first line
	if _, err := r.Read(); err != nil {
		return [][]string{}, err
	}

	records, err := r.ReadAll()

	if err != nil {
		return [][]string{}, err
	}

	return records, nil
}
