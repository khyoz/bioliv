package main

import (
	// "bioliv/global"
	"bioliv/models"
	"bioliv/modules"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
)

type Xelas struct {
	selas     string
	name      string
	street1   string
	street2   string
	cp        string
	city      string
	tel       string
	employees string
}

func main() {
	modules.InitDatabase()
	records, err := readData("selas.csv")
	if err != nil {
		log.Fatal(err)
	}
	db := modules.DBConn
	for _, record := range records {
		xselas := Xelas{
			selas:     record[1],
			name:      record[2],
			street1:   record[3],
			street2:   record[4],
			cp:        record[5],
			city:      record[6],
			tel:       record[7],
			employees: record[8],
		}

		fmt.Printf("insert %s\n", xselas)
		s, _ := strconv.Atoi(xselas.employees)
		aselas := models.Selas{Selas: xselas.selas, Name: xselas.name, Street1: xselas.street1, Street2: xselas.street2, CP: xselas.cp, City: xselas.city, Tel: xselas.tel, Employees: s}
		result := db.Create(&aselas)
		fmt.Println(result)
	}
}

func readData(fileName string) ([][]string, error) {

	f, err := os.Open(fileName)

	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	r := csv.NewReader(f)

	// skip first line
	if _, err := r.Read(); err != nil {
		return [][]string{}, err
	}

	records, err := r.ReadAll()

	if err != nil {
		return [][]string{}, err
	}

	return records, nil
}
