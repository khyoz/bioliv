package main

import (
	// "bioliv/global"
	"bioliv/models"
	"bioliv/modules"
	"encoding/csv"
	"fmt"
	"golang.org/x/text/encoding/charmap"
	"log"
	"os"
	// "strconv"
)

// Firstname  string `gorm:"type:char(255);" json:"firstname" xml:"firstname" form:"firstname" query:"firstname"`
// 	Lastname   string `gorm:"type:char(255);" json:"lastname" xml:"lastname" form:"lastname" query:"lastname"`
// 	Selas      string `gorm:"type:char(255);" json:"selas" xml:"selas" form:"selas" query:"selas"`
// 	Labo       string `type:char(255);" json:"labo" xml:"labo" form:"labo" query:"labo"`
// 	Email      string `gorm:"type:char(255);" json:"email" xml:"email" form:"email" query:"email"`
// 	Registered string `gorm:"type:char(255)" json:"registered" xml:"registered" form:"registered" query:"registered"`

// email;registered;ip;firstname;laboratoire;lastname;lieu;selas
type Inscrits struct {
	email       string
	registered  string
	lastname    string
	ip          string
	firstname   string
	laboratoire string
	lieu        string
	selas       string
}

func main() {
	modules.InitDatabase()
	records, err := readData("Inscrits.csv")
	if err != nil {
		log.Fatal(err)
	}
	db := modules.DBConn
	for _, record := range records {
		xselas := Inscrits{
			email:      record[0],
			registered: record[1],
			// ip:          record[2],
			firstname:   record[3],
			laboratoire: record[4],
			lastname:    record[5],
			// lieu:        record[6],
			selas: record[7]}

		fmt.Printf("insert %s\n", xselas)
		// aselas := models.Inscrits{Selas: xselas.selas, Matricule: xselas.matricule, Lastname: xselas.lastname, Firstname: xselas.firstname, Birthday: xselas.birthday, Sex: xselas.sex, Embauche: xselas.embauche, Anciennete: xselas.anciennete, Contract: xselas.contract, Depart: xselas.depart, Job: xselas.job}
		Aselas := models.Inscrits{Firstname: xselas.firstname, Lastname: xselas.lastname, Selas: xselas.selas, Labo: xselas.laboratoire, Email: xselas.email, Registered: xselas.registered}
		result := db.Create(&Aselas)
		fmt.Println(result)
	}
}

func readData(fileName string) ([][]string, error) {

	f, err := os.Open(fileName)

	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	// r := csv.NewReader(f)
	r := csv.NewReader(charmap.Windows1252.NewDecoder().Reader(f))

	// skip first line
	if _, err := r.Read(); err != nil {
		return [][]string{}, err
	}

	records, err := r.ReadAll()

	if err != nil {
		return [][]string{}, err
	}

	return records, nil
}
