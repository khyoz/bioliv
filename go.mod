module bioliv

go 1.14

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/cosmtrek/air v1.21.2 // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/form3tech-oss/jwt-go v3.2.2+incompatible
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.2.0
	github.com/gofiber/helmet/v2 v2.0.1
	github.com/gofiber/jwt/v2 v2.1.0
	github.com/gofiber/session/v2 v2.0.2
	github.com/gofiber/template v1.6.4
	github.com/google/uuid v1.1.2
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.11.3 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/thomasvvugt/fiber-hashing v0.0.0-20200511145001-a62bb48860d5
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.6
)
