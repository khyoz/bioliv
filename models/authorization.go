package models

import (
	"github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Role model
type Authorization struct {
	ID          uuid.UUID `gorm:"type:char(36);primary_key"`
	Name        string    `gorm:"unique;not null" json:"name" xml:"name" form:"name" query:"name"`
	Description string    `gorm:"type:varchar(100);" json:"description"`
	Read        bool      `gorm:"type:bool;" json:"read"`
	Write       bool      `gorm:"type:bool;" json:"write"`
	Update      bool      `gorm:"type:bool;" json:"update"`
	Delete      bool      `gorm:"type:bool;" json:"delete"`
	Api         bool      `gorm:"type:bool;" json:"api"`
	gorm.Model
}

func (u *Authorization) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4()
	return
}
