package models

import (
	"github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Role model
type Logmail struct {
	ID         uuid.UUID `gorm:"type:char(36);primary_key"`
	Selas      string    `gorm:"unique;not null" json:"selas" xml:"selas" form:"selas" query:"selas"`
	Labo       string    `gorm:"unique;not null" json:"labo" xml:"labo" form:"labo" query:"labo"`
	Lastname   string    `gorm:"type:char(255);" json:"lastname" xml:"lastname" form:"lastname" query:"lastname"`
	Firstname  string    `gorm:"type:char(255);" json:"firstname" xml:"firstname" form:"firstname" query:"firstname"`
	Email      string    `gorm:"type:char(255);" json:"email" xml:"email" form:"email" query:"email"`
	Done       bool
	Comment    string `gorm:"type:text;" json:"comment" xml:"comment" form:"comment" query:"comment"`
	UserFound  bool
	SelasFound bool
	gorm.Model
}

func (u *Logmail) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4()
	return
}
