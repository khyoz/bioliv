package models

type LivraisonMail struct {
	Id        string `json:"id"`
	Firstname string `json:"firstname" xml:"firstname" form:"firstname"`
	Lastname  string `json:"lastname" xml:"lastname" form:"lastname"`
	Email     string `json:"email" xml:"email" form:"email"`
	Labo      string `json:"labo" xml:"labo" form:"labo"`
	Selas     string `json:"selas" xml:"selas" form:"selas"`
}

type MEmployes struct {
	ID        uint
	Selas     string
	Lastname  string
	Firstname string
	Sex       string
}

type MSelas struct {
	ID      uint
	Selas   string
	Name    string
	Street1 string
	Street2 string
	Cp      string
	City    string
	Tel     string
}
