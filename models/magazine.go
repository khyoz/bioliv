package models

import (
	"github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Role model
type Magazine struct {
	ID      uuid.UUID `gorm:"type:char(36);primary_key"`
	Title   string    `gorm:"unique;not null" json:"title" xml:"title" form:"title" query:"title"`
	Except  string    `gorm:"type:text;" json:"except"`
	Content string    `gorm:"type:text;" json:"content"`
	gorm.Model
}

func (u *Magazine) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4()
	return
}
