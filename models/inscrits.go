package models

import (
	// "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Role model
type Inscrits struct {
	// ID        uuid.UUID `gorm:"type:char(36);primary_key"`
	Firstname  string `gorm:"type:char(255);" json:"firstname" xml:"firstname" form:"firstname" query:"firstname"`
	Lastname   string `gorm:"type:char(255);" json:"lastname" xml:"lastname" form:"lastname" query:"lastname"`
	Selas      string `gorm:"type:char(255);" json:"selas" xml:"selas" form:"selas" query:"selas"`
	Labo       string `gorm:"type:char(255);" json:"labo" xml:"labo" form:"labo" query:"labo"`
	Email      string `gorm:"type:char(255);" json:"email" xml:"email" form:"email" query:"email"`
	Registered string `gorm:"type:char(255)" json:"registered" xml:"registered" form:"registered" query:"registered"`

	gorm.Model
}

// func (u *Inscrits) BeforeCreate(tx *gorm.DB) (err error) {
// 	u.ID = uuid.NewV4()
// 	return
// }
