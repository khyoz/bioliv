package models

import (
	"github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type User struct {
	ID         uuid.UUID `gorm:"type:char(36);primary_key"`
	RoleID     uuid.UUID `gorm:"column:role_id" json:"role_id"`
	Email      string
	IsVerified bool
	Pseudo     string
	Password   string
	gorm.Model
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4()
	// if u.Role == "admin" {
	// 	return errors.New("invalid role")
	// }
	return
}
