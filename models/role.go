package models

import (
	"github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Role model
type Role struct {
	ID          uuid.UUID `gorm:"type:char(36);primary_key"`
	Name        string    `gorm:"unique;not null" json:"name" xml:"name" form:"name" query:"name"`
	Description string    `gorm:"type:varchar(100);" json:"description"`
	gorm.Model
}

func (u *Role) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4()
	return
}
