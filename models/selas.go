package models

import (
	// "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Role model
type Selas struct {
	// ID        uuid.UUID `gorm:"type:char(36);primary_key"`
	Selas     string `gorm:"unique;not null" json:"selas" xml:"selas" form:"selas" query:"selas"`
	Name      string `gorm:"unique;not null" json:"name" xml:"name" form:"name" query:"name"`
	Street1   string `gorm:"type:char(255);" json:"street1"`
	Street2   string `gorm:"type:char(255);" json:"street2"`
	CP        string `gorm:"type:char(255);" json:"cp"`
	City      string `gorm:"type:char(255);" json:"city"`
	Tel       string `gorm:"type:char(255);" json:"tel"`
	Employees int    `gorm:"type:char(255);" json:"employees"`
	gorm.Model
}

// func (u *Selas) BeforeCreate(tx *gorm.DB) (err error) {
// 	u.ID = uuid.NewV4()
// 	return
// }
