package models

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/session/v2"
	"github.com/thomasvvugt/fiber-hashing"
)

type App struct {
	*fiber.App
	// DB      *database.Database
	Hasher  hashing.Driver
	Session *session.Session
}

var Application App

// func GetApplication() *App {
// 	return Application
// }
