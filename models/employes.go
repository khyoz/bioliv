package models

import (
	// "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Role model
type Employes struct {
	// ID        uuid.UUID `gorm:"type:char(36);primary_key"`
	Selas      string `gorm:"unique;not null" json:"selas" xml:"selas" form:"selas" query:"selas"`
	Matricule  string `type:char(255);" json:"matricule" xml:"matricule" form:"matricule" query:"matricule"`
	Lastname   string `gorm:"type:char(255);" json:"lastname" xml:"lastname" form:"lastname" query:"lastname"`
	Firstname  string `gorm:"type:char(255);" json:"firstname" xml:"firstname" form:"firstname" query:"firstname"`
	Birthday   string `gorm:"type:char(255);" json:"birthday" xml:"birthday" form:"birthday" query:"birthday"`
	Sex        string `gorm:"type:char(10);" json:"sex" xml:"sex" form:"sex" query:"sex"`
	Embauche   string `gorm:"type:char(10);" json:"embauche" xml:"embauche" form:"embauche" query:"embauche"`
	Anciennete string `gorm:"type:char(10);" json:"anciennete" xml:"anciennete" form:"anciennete" query:"anciennete"`
	Contract   string `gorm:"type:char(30);" json:"contract" xml:"contract" form:"contract" query:"contract"`
	Depart     string `gorm:"type:char(10);" json:"depart" xml:"depart" form:"depart" query:"depart"`
	Job        string `gorm:"type:char(255);" json:"job" xml:"job" form:"job" query:"job"`
	Email      string `gorm:"type:char(255);" json:"email" xml:"email" form:"email" query:"email"`

	gorm.Model
}

// func (u *Selas) BeforeCreate(tx *gorm.DB) (err error) {
// 	u.ID = uuid.NewV4()
// 	return
// }
