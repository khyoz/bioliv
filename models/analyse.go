package models

import (
	"github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Role model
type Analyse struct {
	ID      uuid.UUID `gorm:"type:char(36);primary_key"`
	Title   string    `gorm:"unique;not null" json:"title" xml:"title" form:"title" query:"title"`
	Url     string    `gorm:"type:char(255);" json:"url"`
	Content string    `gorm:"type:text;" json:"content"`
	gorm.Model
}

func (u *Analyse) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4()
	return
}
