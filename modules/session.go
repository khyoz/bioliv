package modules

import (
	"github.com/gofiber/session/v2"
)

var (
	Session *session.Session
)

func InitSession() {
	Session = session.New()
}
