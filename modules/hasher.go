package modules

import (
	"github.com/thomasvvugt/fiber-hashing"
)

var (
	Hasher hashing.Driver
)

func InitHasher() {
	Hasher = hashing.New()
}
