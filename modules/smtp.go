package modules

import (
	"bioliv/models"
	"bytes"
	"crypto/tls"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/satori/go.uuid"
	"gopkg.in/gomail.v2"
	"html/template"
	"log"
	"os"
	"strconv"
)

func SendMail(xtemplate string, to string, params models.LivraisonMail, aUser models.MEmployes, aSelas models.MSelas, refId uuid.UUID) {
	_ = godotenv.Load()
	m := gomail.NewMessage()
	fmt.Println("PARAMS SENDMAIL = ", params)
	fmt.Println("SMTP_FROM = ", os.Getenv("SMTP_FROM"))
	fmt.Println("SMTP_PASSWORD = ", os.Getenv("SMTP_PASSWORD"))
	// m.SetHeader("From", "faq@ichimokuacademy.net")
	m.SetHeader("From", os.Getenv("SMTP_FROM"))
	if os.Getenv("SMTP_TEST") == "X" {
		// fmt.Println("test mode")
		m.SetHeader("To", "khyozsan@gmail.com")
	} else {
		// fmt.Println("real mode")
		m.SetHeader("To", params.Email)
	}
	m.SetHeader("Subject", "Réception de la box !")

	// ------------------------------------------------

	// var out string
	t := template.New("mail")
	// t, _ = template.ParseFiles("./templates/resultlivraison.html")
	t, _ = template.ParseFiles("./templates/mails/mailresultlivraison.html")
	// t, _ = template.ParseFiles("./templates/mail.html")
	fmt.Println("template = ", t)
	// err = t.ExecuteTemplate(out, "T", "<script>alert('you have been pwned')</script>")

	var tpl bytes.Buffer

	type ViewData struct {
		User   models.MEmployes
		Selas  models.MSelas
		Params models.LivraisonMail
		Ref    uuid.UUID
		// Items []Item
	}
	var myData ViewData
	myData.User = aUser
	myData.Selas = aSelas
	myData.Params = params
	myData.Ref = refId

	// if err := t.Execute(&tpl, params); err != nil {
	if err := t.Execute(&tpl, myData); err != nil {
		// if err := t.Execute(&tpl, ); err != nil {
		log.Println(err)
	}

	result := tpl.String()
	// fmt.Println(out, result)

	// ------------------------------------------------
	// m.SetBody("text/html", "Hello <b>Bob</b>!")
	m.SetBody("text/html", result)
	// d := gomail.NewPlainDialer("ichimokuacademy.net", 587, "faq@ichimokuacademy.net", "TzanOPnlTL")
	sport, _ := strconv.Atoi(os.Getenv("SMTP_PORT"))
	d := gomail.NewPlainDialer(os.Getenv("SMTP_HOST"), sport, os.Getenv("SMTP_FROM"), os.Getenv("SMTP_PASSWORD"))
	// d := gomail.NewPlainDialer("moncolis.seminaire-virtuel.com", 587, "moncolis@moncolis.seminaire-virtuel.com", "qWGcP4TEQy")
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}
	fmt.Println("mail sent")
	return
}
