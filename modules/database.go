package modules

import (
	"bioliv/models"
	"fmt"
	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
)

var (
	DBConn *gorm.DB
)

func InitDatabase() {
	var err error
	// var XConn *gorm.DB
	// database.DBConn, err := gorm.Open("")
	_ = godotenv.Load()
	dbuser := os.Getenv("DB_USER")
	dbpwd := os.Getenv("DB_PASSWORD")
	dbhost := os.Getenv("DB_HOST")
	dbport := os.Getenv("DB_PORT")
	dbdatabase := os.Getenv("DB_DATABASE")
	// dsn := "root:@tcp(127.0.0.1:3306)/bioliv?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := dbuser + ":" + dbpwd + "@tcp(" + dbhost + ":" + dbport + ")/" + dbdatabase + "?charset=utf8mb4&parseTime=True&loc=Local"
	DBConn, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database")
	}
	// DBConn = xConn
	fmt.Println("database succesfully connected")
	fmt.Println(DBConn)
	// DBConn.Automigrate(&book.Book{})

	// https://stackoverflow.com/questions/58947804/how-to-pass-dynamic-table-name-in-gorm-model
	// DBConn.AutoMigrate(&Product{table: "jeans"}, &Product{table: "skirts"}, &Product{})
	// DBConn.AutoMigrate(&models.Ichisignal{table: "signals"})
	// DBConn.AutoMigrate(&models.Role{}, &models.User{}, &models.Magazine{}, &models.Analyse{}, &models.Authorization{})
	DBConn.AutoMigrate(&models.Selas{}, &models.Employes{}, &models.Logmail{}, &models.Inscrits{})
	// DBConn.AutoMigrate(&models.Employes{})
	// DBConn.AutoMigrate(&database.User{table: "users"}, &ISignal{table: "signals2"})
	fmt.Println("database migrated")
}

func InitDatabaseLocal() {
	var err error
	// var XConn *gorm.DB
	// database.DBConn, err := gorm.Open("")
	// _ = godotenv.Load()
	// dbuser := os.Getenv("DB_USER")
	// dbpwd := os.Getenv("DB_PASSWORD")
	// dbhost := os.Getenv("DB_HOST")
	// dbport := os.Getenv("DB_PORT")
	// dbdatabase := os.Getenv("DB_DATABASE")
	dsn := "root:@tcp(127.0.0.1:3306)/bioliv?charset=utf8mb4&parseTime=True&loc=Local"
	// dsn := dbuser + ":" + dbpwd + "@tcp(" + dbhost + ":" + dbport + ")/" + dbdatabase + "?charset=utf8mb4&parseTime=True&loc=Local"
	DBConn, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database")
	}
	// DBConn = xConn
	fmt.Println("database succesfully connected")
	fmt.Println(DBConn)
	// DBConn.Automigrate(&book.Book{})

	// https://stackoverflow.com/questions/58947804/how-to-pass-dynamic-table-name-in-gorm-model
	// DBConn.AutoMigrate(&Product{table: "jeans"}, &Product{table: "skirts"}, &Product{})
	// DBConn.AutoMigrate(&models.Ichisignal{table: "signals"})
	// DBConn.AutoMigrate(&models.Role{}, &models.User{}, &models.Magazine{}, &models.Analyse{}, &models.Authorization{})
	DBConn.AutoMigrate(&models.Selas{}, &models.Employes{}, &models.Logmail{}, &models.Inscrits{})
	// DBConn.AutoMigrate(&database.User{table: "users"}, &ISignal{table: "signals2"})
	fmt.Println("database migrated")
}

func CloseDatabase() {

}
