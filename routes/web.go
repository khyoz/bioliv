package routes

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	// "github.com/gofiber/fiber/v2/middleware/basicauth"
	"bioliv/models"
	"bioliv/modules"
	"github.com/satori/go.uuid"
	"github.com/thomasvvugt/fiber-hashing"
	// "github.com/gofiber/session/v2"
	"bytes"
	"encoding/json"
)

func isWebAuthorized(ctx *fiber.Ctx) bool {
	store := modules.Session.Get(ctx)
	userID := store.Get("userid")
	fmt.Println("userid = ", userID)
	if userID == nil {
		return false
	}
	return true
}

func WebSetupRoutes2(zweb *fiber.App) {
	// Unauthenticated route
	zweb.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("WebSetupRoutes2")
	})

	zweb.Get("/login", GetLogin)
	zweb.Post("/login", PostLogin)
	zweb.Get("/register", GetRegister)
	zweb.Post("/register", PostRegister)
	zweb.Get("/error", GetError)

	zweb.Get("/livraison", GetLivraison)
	zweb.Get("/renvoi", GetRenvoi)
	zweb.Get("/inscription", GetRegistration)
	zweb.Post("/webhook", PostWebhook)
	zweb.Post("/webhookjson", PostWebhookjson)
	zweb.Post("/registration", PostRegistration)
	zweb.Post("/livraison", PostLivraison)
	zweb.Post("/renvoi", PostRenvoi)
	// zweb.Use(func(c *fiber.Ctx) error {
	// 	// Set some security headers:
	// 	c.Set("X-XSS-Protection", "1; mode=block")
	// 	c.Set("X-Content-Type-Options", "nosniff")
	// 	c.Set("X-Download-Options", "noopen")
	// 	c.Set("Strict-Transport-Security", "max-age=5184000")
	// 	c.Set("X-Frame-Options", "SAMEORIGIN")
	// 	c.Set("X-DNS-Prefetch-Control", "off")
	// 	fmt.Println("xss X-XSS-Protection")
	// 	// Go to next middleware:
	// 	return c.Next()
	// })

	web := zweb.Group("/", webmidware)
	// web.Use(basicauth.New(basicauth.Config{
	// 	Users: map[string]string{
	// 		"john":  "doe",
	// 		"admin": "123456",
	// 	},
	// 	Realm: "Forbidden",
	// 	Authorizer: func(user, pass string) bool {
	// 		// if user == "john" && pass == "doe" {
	// 		// 	return true
	// 		// }
	// 		// if user == "admin" && pass == "123456" {
	// 		// 	return true
	// 		// }
	// 		return true
	// 	},
	// 	Unauthorized: func(c *fiber.Ctx) error {
	// 		// return c.SendFile("./unauthorized.html")
	// 		return c.Render("error", fiber.Map{})
	// 	},
	// 	ContextUsername: "_user",
	// 	ContextPassword: "_pass",
	// }))

	web.Get("/uuid", GetUuid)
	web.Get("/logout", GetLogout)
	web.Get("/hash/*", GetHash)
	web.Get("/match/*", GetMatch)
	web.Get("/test", GetTest)
	// web := zweb.Group("/")
	// web.Use(webmidware)
	zweb.Use(func(c *fiber.Ctx) error {
		// return c.Status(fiber.StatusNotFound).SendString("404")
		return c.Status(fiber.StatusNotFound).Redirect("/error")
	})
	// fmt.Println(web.Hasher)
}

func webmidware(ctx *fiber.Ctx) error {
	// cfg := basicauth.Config{
	// 	Users: map[string]string{
	// 		"noel": "guth",
	// 		// config.Config("USERNAME"): config.Config("PASSWORD"),
	// 	},
	// }
	// fmt.Println(cfg)
	// err := basicauth.New(cfg)
	// return err
	// ctx.SendString("Unauthenticated. Please signup!")
	// if 1 == 2 {
	if isWebAuthorized(ctx) {
		return ctx.Next()
	} else {
		return ctx.Status(fiber.StatusTeapot).Redirect("/error")
	}
}

// func webmidware(ctx *fiber.Ctx) error {
// 	return ctx.Next()
// }

// func WebSetupRoutes(web *fiber.App) {
// func WebSetupRoutes(web *fiber.App, hasher hashing.Driver) {
// 	// Unauthenticated route
// 	web.Get("/", func(c *fiber.Ctx) error {
// 		return c.SendString("WebSetupRoutes")
// 	})

// 	web.Get("/login", GetLogin)
// 	web.Get("/logout", GetLogout)
// 	web.Get("/hash/*", GetHash)
// 	web.Get("/match/*", GetMatch(hasher))
// 	web.Post("/login", PostLogin)
// 	// fmt.Println(web.Hasher)
// }

func GetUuid(ctx *fiber.Ctx) error {
	u := uuid.NewV4()
	fmt.Println(u)
	return ctx.SendString("uid")
}
func GetLogin(ctx *fiber.Ctx) error {
	if isWebAuthorized(ctx) {
		return ctx.Redirect("/")
	}
	return ctx.Render("login", fiber.Map{})
}
func GetRegister(ctx *fiber.Ctx) error {
	if isWebAuthorized(ctx) {
		return ctx.Redirect("/")
	}
	return ctx.Render("register", fiber.Map{})
}

func GetLogout(ctx *fiber.Ctx) error {
	store := modules.Session.Get(ctx)
	// store.Delete("userid")
	// store.Save()
	if err := store.Destroy(); err != nil {
		panic(err)
	}
	fmt.Println(store)
	return ctx.Render("hello", fiber.Map{})
}

func PostRegister(ctx *fiber.Ctx) error {
	return ctx.SendString("registered")
	// return ctx.Render("hello", fiber.Map{})
}

type Webhook struct {
	Id        string `json:"id"`
	Firstname string `json:"firstname" xml:"firstname" form:"firstname"`
	Lastname  string `json:"lastname" xml:"lastname" form:"lastname"`
	Email     string `json:"email" xml:"email" form:"email"`
	Selas     string `json:"selas" xml:"selas" form:"selas"`
	Labo      string `json:"labo" xml:"labo" form:"labo"`
}

func PostWebhookjson(ctx *fiber.Ctx) error {
	// ctx.Accepts("html")             // "html"
	// ctx.Accepts("text/html")        // "text/html"
	// ctx.Accepts("text/plain")       // "text/html"
	// ctx.Accepts("json", "text")     // "json"
	// ctx.Accepts("application/json") // "application/json"
	ctx.Accepts("application/x-www-form-urlencoded") // "application/x-www-form-urlencoded"
	ctx.Accepts("text/plain;charset=UTF-8")          // "application/x-www-form-urlencoded"
	// ctx.Accepts("json")                              // "application/x-www-form-urlencoded"
	// fmt.Println(ctx.Request)
	fmt.Println("hello PostWebhookjson")
	// fmt.Println("header accept = ", ctx.Get(HeaderAccept))
	// getString(c.fasthttp.Request.Header.ContentType())
	// fmt.Println(ctx.Request.Header.Method())

	abody := ctx.Body()
	r := bytes.NewReader(abody)
	var p Webhook
	err := json.NewDecoder(r).Decode(&p)
	// fmt.Println("abody => ", abody)

	if 1 == 2 {
		fmt.Println("p => ", p, " err = ", err)
		fmt.Println("id => ", ctx.Hostname())
		// fmt.Println("id => ", ctx.faBodyInflate())
		fmt.Println("request => ", ctx.Request())
		// fmt.Println("body => ", ctx.Body())
		fmt.Println("------------------------------")
		fmt.Println("------------------------------")
		// fmt.Println("Content-Type => ", ctx.Get("firstname"))
		fmt.Println("Content-Length => ", ctx.Get("Content-Length"))
		fmt.Println("------------------------------")
		// fmt.Println(ctx.Body())
		// fmt.Println("id = ", ctx.Params("id"))
		fmt.Println("request END => ")
	}

	// params := new(models.LivraisonMail)
	var params models.LivraisonMail
	params.Id = p.Id
	params.Firstname = p.Firstname
	params.Lastname = p.Lastname
	params.Email = p.Email
	params.Selas = p.Selas
	params.Labo = p.Labo

	var aUser models.MEmployes
	var aSelas models.MSelas
	db := modules.DBConn
	// db.Where("lastname = ?", ulastname).Where("firstname = ?", ufirstname).Limit(1).Find(&aUser)
	db.Table("employes").Where("lastname = ?", params.Lastname).Where("firstname = ?", p.Firstname).Limit(1).Find(&aUser)
	db.Table("selas").Where("name = ?", aUser.Selas).Limit(1).Find(&aSelas)
	if aSelas.ID == 0 {
		db.Table("selas").Where("selas = ?", aUser.Selas).Limit(1).Find(&aSelas)
	}
	fmt.Println("user = ", aUser)
	fmt.Println("selas = ", aSelas, " for ", aUser.Selas)

	fmt.Println("params = ", params)

	fmt.Println("params => DEBUG", p)
	var userfound bool
	var selasfound bool
	userfound = false
	selasfound = false
	if aUser.ID != 0 {
		userfound = true
	}
	if aSelas.ID != 0 {
		selasfound = true
	}
	//  save log
	logmail := models.Logmail{Firstname: params.Firstname, Lastname: params.Lastname,
		Email: params.Email, Selas: params.Selas, Labo: params.Labo, SelasFound: selasfound, UserFound: userfound}
	result := db.Create(&logmail)
	fmt.Println("result = ", result, " id = ", logmail.ID)
	if 1 == 1 {
		modules.SendMail("", "", params, aUser, aSelas, logmail.ID)
	}
	// if err := ctx.BodyParser(params); err != nil {
	// 	return ctx.JSON(fiber.Map{
	// 		"status": "error1",
	// 		"data":   err.Error(),
	// 	})
	// } else {

	return ctx.JSON(fiber.Map{
		"status": "success",
		// "data":   params,
		"data": p,
	})
	// }
	return ctx.SendString("webhookjson ok")
}

func PostWebhookjson2(ctx *fiber.Ctx) error {
	// ctx.Accepts("html")             // "html"
	// ctx.Accepts("text/html")        // "text/html"
	// ctx.Accepts("text/plain")       // "text/html"
	// ctx.Accepts("json", "text")     // "json"
	// ctx.Accepts("application/json") // "application/json"
	fmt.Println(ctx.Body())
	fmt.Println("hello PostWebhookjson2")
	params := new(Webhook)
	// if err := ctx.BodyParser(&params); err != nil {
	if err := ctx.BodyParser(params); err != nil {
		return ctx.JSON(fiber.Map{
			"status": "error",
			"data":   err.Error(),
		})
	} else {
		fmt.Println("param ", params.Firstname)
		return ctx.JSON(fiber.Map{
			"status": "success",
			"data":   params,
		})
	}
	return ctx.SendString("webhookjson ok")
}

func PostWebhook(ctx *fiber.Ctx) error {

	// var awebhook webhook
	awebhook := new(Webhook)
	fmt.Println(ctx.Body())
	ctx.BodyParser(awebhook)
	fmt.Println("webhook data = ", awebhook)

	// var e Event
	// var sbody []byte
	// sbody = ctx.Body()
	// if err := json.NewDecoder(sbody).Decode(&awebhook); err != nil {
	// http.Error(w, "Cannot parse data!", http.StatusBadRequest)
	// return
	// }
	// w.WriteHeader(http.StatusOK)

	return ctx.SendString("webhook ok")
}

func GetLivraison(ctx *fiber.Ctx) error {
	return ctx.Render("livraison", fiber.Map{})
}

func GetRenvoi(ctx *fiber.Ctx) error {
	return ctx.Render("renvoi", fiber.Map{})
}

func GetRegistration(ctx *fiber.Ctx) error {
	return ctx.Render("registration", fiber.Map{})
}

// type Employes struct {
// 	ID        uint
// 	Selas     string
// 	Lastname  string
// 	Firstname string
// 	Sex       string
// }

// type Selas struct {
// 	ID      uint
// 	Selas   string
// 	Name    string
// 	Street1 string
// 	Street2 string
// 	Cp      string
// 	City    string
// 	Tel     string
// }

func PostRenvoi(ctx *fiber.Ctx) error {
	uidlog := ctx.FormValue("idlog")
	umail := ctx.FormValue("email")
	fmt.Println("id = ", uidlog, " mail = ", umail)
	db := modules.DBConn
	var aLog models.Logmail
	db.Table("logmails").Where("id = ?", uidlog).Limit(1).Find(&aLog)
	if aLog.Lastname == "" {
		return ctx.SendString("id " + uidlog + " non trouvé :o(")
	}

	var aUser models.MEmployes
	var aSelas models.MSelas
	var aInscrit models.Inscrits
	db.Table("inscrits").Where("email = ?", aLog.Email).Limit(1).Find(&aInscrit)
	if aInscrit.ID == 0 {
		db.Table("inscrits").Where("lastname = ?", aLog.Lastname).Where("firstname = ?", aLog.Firstname).Limit(1).Find(&aInscrit)
	}
	if aInscrit.ID == 0 {
		db.Table("inscrits").Where("lastname = ?", aLog.Firstname).Where("firstname = ?", aLog.Lastname).Limit(1).Find(&aInscrit)
	}
	fmt.Println("inscrit = ", aInscrit)
	db.Table("employes").Where("lastname = ?", aLog.Lastname).Where("firstname = ?", aLog.Firstname).Limit(1).Find(&aUser)
	if aUser.ID == 0 {
		db.Table("employes").Where("lastname = ?", aLog.Firstname).Where("firstname = ?", aLog.Lastname).Limit(1).Find(&aUser)
	}
	// db.Where("name = ?", aUser.Selas).Limit(1).Find(&aSelas)
	db.Table("selas").Where("name = ?", aUser.Selas).Limit(1).Find(&aSelas)
	if aSelas.ID == 0 {
		db.Table("selas").Where("selas = ?", aUser.Selas).Limit(1).Find(&aSelas)
	}
	fmt.Println("user = ", aUser)
	fmt.Println("selas = ", aSelas, " for ", aUser.Selas)
	var params models.LivraisonMail
	params.Id = ""
	params.Firstname = aLog.Firstname
	params.Lastname = aLog.Lastname
	params.Email = umail
	fmt.Println(umail)
	if umail == "khyozsan@gmail.com" {
		return ctx.Render("resultInscription", fiber.Map{"user": aUser, "selas": aSelas, "params": params, "inscrit": aInscrit})
	}
	if 1 == 1 {
		// xuid := uuid.NewV4()
		xuid := aLog.ID
		modules.SendMail("", "", params, aUser, aSelas, xuid)
		db.Model(&aLog).Update("done", true)
	}
	return ctx.SendString("OK")
}
func PostRegistration(ctx *fiber.Ctx) error {
	ufirstname := ctx.FormValue("firstname")
	ulastname := ctx.FormValue("lastname")
	// uselas := ctx.FormValue("selas")
	uemail := ctx.FormValue("email")
	// ulabo := ctx.FormValue("labo")

	var aUser models.MEmployes
	var aSelas models.MSelas
	var aInscrit models.Inscrits

	db := modules.DBConn
	db.Table("inscrits").Where("email = ?", uemail).Limit(1).Find(&aInscrit)
	if aInscrit.ID == 0 {
		db.Table("inscrits").Where("lastname = ?", ulastname).Where("firstname = ?", ufirstname).Limit(1).Find(&aInscrit)
	}
	if aInscrit.ID == 0 {
		db.Table("inscrits").Where("lastname = ?", ufirstname).Where("firstname = ?", ulastname).Limit(1).Find(&aInscrit)
	}
	fmt.Println("inscrit = ", aInscrit)
	db.Table("employes").Where("lastname = ?", ulastname).Where("firstname = ?", ufirstname).Limit(1).Find(&aUser)
	if aUser.ID == 0 {
		db.Table("employes").Where("lastname = ?", ufirstname).Where("firstname = ?", ulastname).Limit(1).Find(&aUser)
	}
	// db.Where("name = ?", aUser.Selas).Limit(1).Find(&aSelas)
	db.Table("selas").Where("name = ?", aUser.Selas).Limit(1).Find(&aSelas)
	if aSelas.ID == 0 {
		db.Table("selas").Where("selas = ?", aUser.Selas).Limit(1).Find(&aSelas)
	}
	fmt.Println("user = ", aUser)
	fmt.Println("selas = ", aSelas, " for ", aUser.Selas)
	var params models.LivraisonMail
	// params.Id = p.Id
	params.Firstname = ufirstname
	params.Lastname = ulastname
	// params.Selas = uselas
	// params.Labo = ulabo
	params.Email = uemail
	fmt.Println("params = ", params)
	// return ctx.Render("resultlivraison", fiber.Map{"user": aUser, "selas": aSelas, "ulastname": ulastname, "ufirstname": ufirstname, "uselas": uselas, "uemail": uemail})
	return ctx.Render("resultInscription", fiber.Map{"user": aUser, "selas": aSelas, "params": params, "inscrit": aInscrit})
}

func PostLivraison(ctx *fiber.Ctx) error {
	ufirstname := ctx.FormValue("firstname")
	ulastname := ctx.FormValue("lastname")
	uselas := ctx.FormValue("selas")
	uemail := ctx.FormValue("email")
	ulabo := ctx.FormValue("labo")

	var aUser models.MEmployes
	var aSelas models.MSelas

	db := modules.DBConn
	db.Table("employes").Where("lastname = ?", ulastname).Where("firstname = ?", ufirstname).Limit(1).Find(&aUser)
	// db.Where("name = ?", aUser.Selas).Limit(1).Find(&aSelas)
	db.Table("selas").Where("name = ?", aUser.Selas).Limit(1).Find(&aSelas)
	if aSelas.ID == 0 {
		db.Table("selas").Where("selas = ?", aUser.Selas).Limit(1).Find(&aSelas)
	}
	fmt.Println("user = ", aUser)
	fmt.Println("selas = ", aSelas, " for ", aUser.Selas)
	var params models.LivraisonMail
	// params.Id = p.Id
	params.Firstname = ufirstname
	params.Lastname = ulastname
	params.Selas = uselas
	params.Labo = ulabo
	params.Email = uemail
	fmt.Println("params = ", params)
	// return ctx.Render("resultlivraison", fiber.Map{"user": aUser, "selas": aSelas, "ulastname": ulastname, "ufirstname": ufirstname, "uselas": uselas, "uemail": uemail})
	return ctx.Render("resultlivraison", fiber.Map{"user": aUser, "selas": aSelas, "params": params})
}

func PostLogin(ctx *fiber.Ctx) error {
	User := new(models.User)
	username := ctx.FormValue("username")
	fmt.Println("pseudo = ", username)
	// password := ctx.FormValue("password")
	fmt.Println(modules.DBConn, " and ", User, " and ", username)
	// // if response := database.DBConn.Where("id = ?", id).First(&User); response.Error != nil {
	if response := modules.DBConn.Where("pseudo = ?", username).First(&User); response.Error != nil {
		fmt.Println("ERROR DATABASE LOGIN")
		// 	// return nil, response.Error
	}
	store := modules.Session.Get(ctx)
	store.Set("userid", User.ID)
	defer store.Save()
	return ctx.Render("hello", fiber.Map{})
}
func GetError(ctx *fiber.Ctx) error {

	return ctx.Render("error", fiber.Map{})
}

// func GetMatch(ctx *fiber.Ctx) error {
func GetMatch(ctx *fiber.Ctx) error {
	match, _ := modules.Hasher.MatchHash("noelguth", ctx.Params("*"))
	// match, _ := xhasher.MatchHash("noelguth", ctx.Params("*"))
	if match {
		return ctx.SendString("Matches!")
	}
	return ctx.SendString("Does not match :c")
}

func GetTest(ctx *fiber.Ctx) error {
	store := modules.Session.Get(ctx)
	userID := store.Get("userid")
	fmt.Println("user id = ", userID)
	return ctx.SendString("test")
}
func GetHash(ctx *fiber.Ctx) error {

	hasher := hashing.New()
	// hash, _ := hasher.CreateHash("Password")
	hash, _ := hasher.CreateHash(ctx.Params("*"))
	return ctx.SendString(hash)
}
