package web

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/session/v2"
)

func ShowLoginForm() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		err := ctx.Render("login", fiber.Map{})
		if err != nil {
			if err2 := ctx.Status(500).SendString(err.Error()); err2 != nil {
				panic(err2.Error())
			}
		}
		return err
	}
}
