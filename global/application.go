package global

import (
	"github.com/gofiber/fiber/v2"
	// "github.com/gofiber/session/v2"
	"github.com/gofiber/template/html"
	// "github.com/thomasvvugt/fiber-hashing"
	// "bioliv/database"
	// "fmt"
	// "github.com/gofiber/session/v2"
	// "bioliv/models"
	"bioliv/routes"
	"github.com/gofiber/helmet/v2"
)

type App struct {
	*fiber.App
	// DB      *database.Database
	// Hasher  hashing.Driver
	// Session *session.Session
}

// var Xapp App

func InitApplication() *App {
	engine := html.New("./templates", ".html")

	xapp := fiber.New(fiber.Config{
		Views: engine,
	})
	xapp.Static("/public", "./public")
	xapp.Use(helmet.New())
	// store := session.New()

	// hasher := hashing.New()

	Xapp := &App{
		App: xapp,
		// Hasher:  hasher,
		// Session: store,
	}

	// models.Application = *Xapp
	// fmt.Println(Xapp.App)
	// fmt.Println(Xapp.Hasher)
	// routes.WebSetupRoutes(xapp, hasher)
	routes.WebSetupRoutes2(xapp)
	return Xapp
}
