package main

// https://github.com/gofiber/jwt
// https://docs.gofiber.io/api/ctx#cookies
import (
	"fmt"
	// "github.com/gofiber/fiber/v2"
	// "github.com/gofiber/template/html"
	"bioliv/modules"
	// "bioliv/routes"
	"bioliv/global"
	// "time"
	// jwt "github.com/form3tech-oss/jwt-go"
	// jwtware "github.com/gofiber/jwt/v2"
	// "github.com/joho/godotenv"
	"github.com/joho/godotenv"
	"os"
)

var sSecret string

// func setupRoutes(app *fiber.App) {
// 	// Unauthenticated route
// 	app.Get("/", accessible)
// 	app.Post("/login", login)
// 	app.Get("/logout", logout)

// 	app.Use(jwtware.New(jwtware.Config{
// 		// SigningKey: []byte("secret"),
// 		SigningKey: []byte(sSecret),
// 	}))

// 	app.Get("/restricted", restricted)
// }

func main() {
	// getSecretJWT()

	// engine := html.New("./templates", ".html")
	// app := fiber.New(fiber.Config{
	// 	Views: engine,
	// })
	// app.Static("/public", "./public")
	// session.InitApp()
	// app := session.Xapp
	_ = godotenv.Load()
	app := global.InitApplication()
	// app.Get("/form", func(c *fiber.Ctx) {
	// 	c.Fasthttp.Request.Header.Method()
	// 	// => []byte("GET")

	// 	c.Fasthttp.Response.Write([]byte("Hello, World!"))
	// 	// => "Hello, World!"
	// })
	modules.InitDatabase()
	modules.InitSession()
	modules.InitHasher()

	defer modules.CloseDatabase()

	// setupRoutes(app)
	// routes.WebSetupRoutes(app.App)

	// app.Listen("localhost:3000")
	app.Listen(os.Getenv("APP_DOMAIN") + ":" + os.Getenv("APP_PORT"))
	fmt.Println(" init ok")
}

// func login(c *fiber.Ctx) error {
// 	user := c.FormValue("user")
// 	pass := c.FormValue("pass")

// 	// Throws Unauthorized error
// 	if user != "john" || pass != "doe" {
// 		return c.SendStatus(fiber.StatusUnauthorized)
// 	}

// 	// Create token
// 	token := jwt.New(jwt.SigningMethodHS256)

// 	// Set claims
// 	claims := token.Claims.(jwt.MapClaims)
// 	claims["name"] = "John Doe"
// 	claims["admin"] = false
// 	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

// 	// Generate encoded token and send it as response.
// 	// t, err := token.SignedString([]byte("secret"))
// 	t, err := token.SignedString([]byte(sSecret))
// 	if err != nil {
// 		return c.SendStatus(fiber.StatusInternalServerError)
// 	}

// 	cookie := new(fiber.Cookie)
// 	cookie.Name = "token"
// 	cookie.Value = t
// 	cookie.Domain = os.Getenv("DOMAIN")
// 	cookie.Expires = time.Now().Add(24 * time.Hour)

// 	return c.JSON(fiber.Map{"token": t, "cookie": cookie})
// }
// func logout(c *fiber.Ctx) error {
// 	// fmt.Println(c.Locals)
// 	// c.Locals("user", nil)
// 	fmt.Println(c.Locals)
// 	cookie := new(fiber.Cookie)
// 	cookie.Name = "token"
// 	cookie.Value = ""
// 	cookie.Domain = os.Getenv("DOMAIN")
// 	cookie.MaxAge = -1
// 	// return c.SendString("logout")
// 	return c.JSON(fiber.Map{"cookie": cookie})
// }
// func accessible(c *fiber.Ctx) error {
// 	return c.SendString("Accessible")
// }

// func restricted(c *fiber.Ctx) error {
// 	user := c.Locals("user").(*jwt.Token)
// 	claims := user.Claims.(jwt.MapClaims)
// 	name := claims["name"].(string)
// 	return c.SendString("Welcome " + name)
// }

// func getSecretJWT() {
// 	e := godotenv.Load()
// 	if e != nil {
// 		panic(e.Error())
// 	}
// 	sSecret = os.Getenv("JWT_SECRET")
// }
